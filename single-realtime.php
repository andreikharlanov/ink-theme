<?php
/**
 * The Template for displaying all single posts.
 *
 * @package Stag_Customizer
 * @subpackage Ink
 */

get_header(); ?>


<div class="article-realtime-cover" style="height: 128px; background-color:black;">
</div>
	<main id="main" class="site-main">
		<article>
		<div class="entry-content-realtime entry-content">

	<?php while ( have_posts() ) : the_post(); ?>
		
		<!-- <h2 style="margin-bottom: 0px;"><a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2> -->
		<div class="realtime-date"><a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_time('g:i'); ?></a></div>
		<?php the_content(); ?>
		<?php get_template_part( '_post', 'comments' ); ?>

	<?php endwhile; // end of the loop. ?>
	</div>
</article>

	</main><!-- #main -->

<?php get_footer();