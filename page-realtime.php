<?php
/**
 * Template Name: Realtime Category Page
 */

// Get selected sidebar
$sidebar = stag_get_post_meta( 'settings', get_the_ID(), 'page-sidebar' );

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<div class="entry-content-realtime entry-content">
			<?php 
			// the query to set the posts per page to 3
			$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
			$args = array('posts_per_page' => 10, 'paged' => $paged, 'cat' => 2);
			query_posts($args); ?>
			<!-- the loop -->
			<?php if ( have_posts() ) : while (have_posts()) : the_post(); ?>
					<!-- rest of the loop -->
					<!-- the title, the content etc.. -->
					<!-- don't need titles
						<h2 style="margin-bottom: 0px; font-size: 16px;"><a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2> -->
					<div class="realtime-date"><a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_time('g:i'); ?></a></div>
					 	<div class="entry-realtime">
					 		<?php the_content(); ?>
					 		
					 	</div>
			<?php endwhile; ?>
			<!-- pagination -->
			<?php next_posts_link(); ?>
			<?php previous_posts_link(); ?>
			<?php else : ?>
			<!-- No posts found -->
			<?php endif; ?>
		</div>
		</main><!-- #main -->
	</div><!-- #primary -->
		



	<?php if ( '' != $sidebar ) : ?>
	<section id="<?php echo esc_attr( $sidebar ); ?>" class="stag-custom-widget-area">
		<?php dynamic_sidebar( $sidebar ); ?>
	</section>
	<?php endif; ?>
	
	


<?php get_footer(); ?>